# BicoFácil API #

Node.js API for BicoFácil App.

### Version ###

* 1.0.0

### How to run ###
* Install all dependencies with
```shell 
> yarn install | npm install
```
* Run the following command
```shell
> yarn dev | npm dev
```

### Endpoints ###

* [Swagger](https://swagger.io)

### Authors ###

* [Christopher Moura](https://github.com/ccmoura)
* [Vinicius Lemos](https://github.com/ViniciusBLemos)