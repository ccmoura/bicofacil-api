import express from 'express';

import multer from 'multer';
import multerConfig from './config/multer';
import authMiddleware from './app/middlewares/auth';
import bruteForce from './app/middlewares/bruteforce';

import FileController from './app/controllers/FileController';
import ClientController from './app/controllers/ClientController';
import SessionController from './app/controllers/SessionController';
import ProviderController from './app/controllers/ProviderController';
import ServiceController from './app/controllers/ServiceController';

const routes = express.Router();
const upload = multer(multerConfig);

/* File routes */
routes.post('/files', upload.single('file'), FileController.store);

/* Login routes */
routes.post('/login', bruteForce.prevent, SessionController.store);

/* Client routes */
routes.post('/clients', ClientController.store);
routes.get('/clients', ClientController.index);
routes.get('/clients/:id', ClientController.show);
routes.put('/clients', authMiddleware, ClientController.update);
routes.delete('/clients/:id', ClientController.delete);

/* Provider routes */
routes.post('/providers', ProviderController.store);
routes.get('/providers', ProviderController.index);
routes.get('/providers/:id', ProviderController.show);
routes.get('/providers/search', ProviderController.search);
routes.put('/providers', authMiddleware, ProviderController.update);
routes.delete('/providers/:id', ProviderController.delete);

/* Service routes
routes.post('/services', ServiceController.store);
routes.get('/services', ServiceController.index);
routes.get('/services/:id', ServiceController.show);
routes.get('/services/search', ServiceController.search);
routes.put('/services', authMiddleware, ServiceController.update);
routes.delete('/services/:id', ServiceController.delete);
*/
/* Rating routes */
/* Candidacy routes */

/* Test routes */
routes.get('/ping', function (request, response) {
  response.status(200).json({ message: 'pong' });
});

export default routes;
