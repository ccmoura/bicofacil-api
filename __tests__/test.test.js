import request from 'supertest';
import app from '../src/app';

describe('test routes', () => {
  it('must return pong', async () => {
    const response = await request(app).get('/bicofacil/ping');
    expect(response.status).toBe(200);
    expect(response.body.message).toBe('pong');
  });
});
