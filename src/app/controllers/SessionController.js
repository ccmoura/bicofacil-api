import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import models from '../models';
import login from '../../config/login';

const Provider = models.Provider;
const Client = models.Client;

class ProviderSessionController {
  async store(request, response) {
    const { email, password } = request.body;
    let isClient = false;
    const user = await Provider.findOne({ where: { email } });

    if (!user) {
      user = await Client.findOne({ where: { email } });
      if (!user) {
        return response.status(401).json({ error: 'User not found' });
      } else {
        isClient = true;
      }
    }

    const passwordMatches = await bcrypt.compare(password, user.password);

    if (!passwordMatches) {
      return response.status(401).json({ error: 'Password does not match' });
    }

    const { id, name } = user;

    return response.json({
      user: {
        id,
        name,
        email,
      },
      token: jwt.sign({ id, isClient }, login.secret, {
        expiresIn: login.expiresIn,
      }),
    });
  }
}

export default new ProviderSessionController();
