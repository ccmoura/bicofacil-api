import './bootstrap';

import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import ratelimit from './app/middlewares/ratelimit';
import routes from './routes.js';

const app = express();

app.use(
  cors({
    origin:
      process.env.NODE_ENV.trim() === 'development' ||
      process.env.NODE_ENV.trim() === 'test'
        ? false
        : process.env.FRONT_URL,
  })
);

app.use(express.json());

if (process.env.NODE_ENV.trim() !== 'development') app.use(ratelimit);

app.disable('x-powered-by');

app.use(helmet());

app.use('/bicofacil', routes);

export default app;
