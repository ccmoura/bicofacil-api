import request from 'supertest';
import app from '../../src/app';
var token;
describe('provider routes', () => {
  it('should be able to create the provider', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'sass@gmail.com',
      password: 'AmoMeuProfessor',
      repeat_password: 'AmoMeuProfessor',
      cpf: '41255833076',
      cep: '96015280',
      complement: 'Casa F',
      house_number: 1830,
      birth_date: '19/10/2002',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body).toHaveProperty('roles');
  });

  it('should be able to update password', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
      available: true,
      old_password: 'AmoMeuProfessor',
      new_password: 'senhadocarlota2021',
      repeat_new_password: 'senhadocarlota2021',
    });
    expect(response.status).toBe(200);
  });

  it('should not be able to update password', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
      available: true,
      old_password: 'AmoMeuProfessor',
      new_password: 'senhadocarlota2022',
      repeat_new_password: 'senhadocarlota2022',
    });
    expect(response.status).toBe(401);
    expect(response.body.error).toBe('Old password does not match');
  });
  it('should be able to update cep', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
      available: true,
    });
    expect(response.status).toBe(200);
    expect(response.body.cep).toBe('96010750');
  });

  it('should not be able to update role', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de a',
      per_hour: 45.5,
      available: true,
    });
    expect(response.status).toBe(400);
    expect(response.body.error).toBe('Invalid role');
  });

  it('should not be able to update by experience', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      experience: -1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
      available: true,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should not be able to update by city', async () => {
    const response = await request(app).put('/bicofacil/providers/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96224240',
      complement: 'Casa 4w',
      house_number: 150,
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
      available: true,
    });
    expect(response.status).toBe(400);
    expect(response.body.error).toBe('City not accepted');
  });

  it('should show info about provider', async () => {
    const response = await request(app).get('/bicofacil/providers/1');
    expect(response.status).toBe(200);
  });

  it('should not show info about provider', async () => {
    const response = await request(app).get('/bicofacil/providers/15');
    expect(response.status).toBe(404);
  });

  it('should find first provider by substring on name', async () => {
    const response = await request(app).get(
      '/bicofacil/providers/search?filter=carl'
    );
    expect(response.status).toBe(200);
    expect(response.body[0].name).toBe('Carlos');
  });

  it('should find first provider by substring on role', async () => {
    const response = await request(app).get(
      '/bicofacil/providers/search?filter=marido'
    );
    expect(response.status).toBe(200);
    expect(response.body[0].name).toBe('Carlos');
  });

  it('should not find any provider by name', async () => {
    const response = await request(app).get(
      '/bicofacil/providers/search?filter=jonas'
    );
    expect(response.status).toBe(404);
  });

  it('should not find any provider by role', async () => {
    const response = await request(app).get(
      '/bicofacil/providers/search?filter=frete'
    );
    expect(response.status).toBe(404);
  });

  it('should present email exists error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'sass@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '41255833076',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('Email already exists');
  });

  it('should present cpf exists error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '41255833076',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('CPF already exists');
  });

  it('should present city not accepted error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '15421387011',
      cep: '20011901',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('City not accepted');
  });

  it('should present age error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '15421387011',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/2005',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe(
      'Age must be greater than or equal to 18 years'
    );
  });

  it('should present role error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '15421387011',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
      experience: 1,
      roles: 'instalador de ar-dicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('Invalid role');
  });

  it('should list all registered providers', async () => {
    const response = await request(app).get('/bicofacil/providers');

    expect(response.status).toBe(200);
  });

  it('should present invalid cpf error', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlossva@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '02000500020',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
      experience: 1,
      roles: 'instalador de ar-condicionado;marido de aluguel',
      per_hour: 45.5,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('Invalid CPF');
  });

  it('should delete the first provider', async () => {
    const response = await request(app).delete('/bicofacil/providers/1');

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('result');
  });

  it('should present schema error on password', async () => {
    const response = await request(app).post('/bicofacil/providers').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carloss1341va@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'snhadocarlota',
      cpf: '68077268056',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
  });
});
