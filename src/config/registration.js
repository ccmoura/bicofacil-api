export default {
  name: {
    min: 2,
    max: 45,
  },
  surname: {
    min: 1,
    max: 60,
  },
  email: {
    min: 9,
    max: 320,
  },
  password: {
    min: 5,
    max: 32,
  },
  address: {
    max: 70,
  },
  cities: ['Pelotas'],
  roles: ['frete', 'marido de aluguel', 'instalador de ar-condicionado'],
};
