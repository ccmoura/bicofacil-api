'use strict';
module.exports = (sequelize, DataTypes) => {
  const Candidacies = sequelize.define(
    'Candidacies',
    {
      serviceId: DataTypes.INTEGER,
      providerId: DataTypes.INTEGER,
      value: DataTypes.FLOAT,
      message: DataTypes.STRING,
    },
    {}
  );
  Candidacies.associate = function (models) {};
  return Candidacies;
};
