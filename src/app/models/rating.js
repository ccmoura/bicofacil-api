'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rating = sequelize.define(
    'Rating',
    {
      stars: DataTypes.DECIMAL,
      message: DataTypes.STRING,
    },
    {}
  );
  Rating.associate = function (models) {};
  return Rating;
};
