# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v3.0.0...v3.0.1) (2020-06-27)

## [3.0.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v2.0.1...v3.0.0) (2020-06-24)


### ⚠ BREAKING CHANGES

* **provider/client:** clients and providers now don't register the same email

* **provider/client:** client / provider register ([8b38b8f](https://bitbucket.org/ccmoura/bicofacil-api/commit/8b38b8fe90935aca2fb3d447b30057328c111518))

### [2.0.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v2.0.0...v2.0.1) (2020-05-31)


### Bug Fixes

* **databases:** set database from docker image and fix in controllers ([a2cdb7a](https://bitbucket.org/ccmoura/bicofacil-api/commit/a2cdb7abb42bd3278d2ea8677be479134afed9d3))

## [2.0.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.20.4...v2.0.0) (2020-05-31)


### ⚠ BREAKING CHANGES

* **login:** SessionController find users in two tables.

* **login:** refactor login for client and provider ([db4bedf](https://bitbucket.org/ccmoura/bicofacil-api/commit/db4bedfb3a3f3324e6c62bd82fce72bf998c8bb2))

### [1.20.4](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.20.3...v1.20.4) (2020-05-28)

### [1.20.3](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.20.2...v1.20.3) (2020-04-21)


### Bug Fixes

* **routes.js:** fixing error on routes.js ([dca15d1](https://bitbucket.org/ccmoura/bicofacil-api/commit/dca15d1c594b809caa4c84a24799d1ff0171c1a6))

### [1.20.2](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.20.1...v1.20.2) (2020-04-21)

### [1.20.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.20.0...v1.20.1) (2020-04-20)

## [1.20.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.18.0...v1.20.0) (2020-04-20)


### Features

* **routes_provider:** routes have been implemented provider ([1db8651](https://bitbucket.org/ccmoura/bicofacil-api/commit/1db86519628741b42e0311d150d75dbd391dfa13))
* **search-providers:** providers search resource & birth_date refactor ([3f0f8ae](https://bitbucket.org/ccmoura/bicofacil-api/commit/3f0f8ae67584e23e934c22bfd222d845a55e5df1))


### Bug Fixes

* **fixed bugs.:** fixed rout show and updade ([5722465](https://bitbucket.org/ccmoura/bicofacil-api/commit/57224655c86c997060838c8da179e204934ca752))

## [1.19.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.14.0...v1.19.0) (2020-04-19)


### Features

* **development branch:** inserting develop branch ([752834a](https://bitbucket.org/ccmoura/bicofacil-api/commit/752834a570633b2c0d41822fabdffc2a83498d55))
* **routes_provider:** routes have been implemented provider ([1db8651](https://bitbucket.org/ccmoura/bicofacil-api/commit/1db86519628741b42e0311d150d75dbd391dfa13))
* **security:** adding redis, helmet, cors, brute and rate-limit ([f002846](https://bitbucket.org/ccmoura/bicofacil-api/commit/f002846fc3cb3898c8750441066641d9c2e443d3))
* **tests:** jest config ([7abf9e5](https://bitbucket.org/ccmoura/bicofacil-api/commit/7abf9e5d0fd9148d265346e3708c7b737ba9ab25))


### Bug Fixes

* **fixed bugs.:** fixed rout show and updade ([5722465](https://bitbucket.org/ccmoura/bicofacil-api/commit/57224655c86c997060838c8da179e204934ca752))

## [1.18.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.17.0...v1.18.0) (2020-04-14)


### Features

* **development branch:** inserting develop branch ([752834a](https://bitbucket.org/ccmoura/bicofacil-api/commit/752834a570633b2c0d41822fabdffc2a83498d55))

## [1.17.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.16.2...v1.17.0) (2020-04-14)


### Features

* **security:** adding redis, helmet, cors, brute and rate-limit ([f002846](https://bitbucket.org/ccmoura/bicofacil-api/commit/f002846fc3cb3898c8750441066641d9c2e443d3))

### [1.16.2](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.16.1...v1.16.2) (2020-04-13)

### [1.16.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.16.0...v1.16.1) (2020-04-13)

## [1.16.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.15.0...v1.16.0) (2020-04-10)


### Features

* **tests:** jest config ([7abf9e5](https://bitbucket.org/ccmoura/bicofacil-api/commit/7abf9e5d0fd9148d265346e3708c7b737ba9ab25))

## [1.15.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.13.0...v1.15.0) (2020-04-09)


### Features

* **update_client:** implementing rout put ([d321d8b](https://bitbucket.org/ccmoura/bicofacil-api/commit/d321d8b141dfe7ad62a849f9b813037863256d1c))

## [1.14.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.8.0...v1.14.0) (2020-04-09)


### Features

* **auth.js:** auth middleware for user authorization ([0461bd1](https://bitbucket.org/ccmoura/bicofacil-api/commit/0461bd1eb3614d0542638e44f89ad5eab1feddb7))
* **client-address:** new columns for cliente address and cep validation ([6ef7797](https://bitbucket.org/ccmoura/bicofacil-api/commit/6ef77977011259fe50c2d702154aeb733b8d39d8))
* **file-url:** get file url ([71f715b](https://bitbucket.org/ccmoura/bicofacil-api/commit/71f715b407eff83e6205e7aaf093d48fb72eb97f))
* **merge:** merge development - ([aa45d20](https://bitbucket.org/ccmoura/bicofacil-api/commit/aa45d200189cc00fbd127cbe1aff723d638bf3a1))
* **provider-store:** i created the function that stores a provider ([e6f770b](https://bitbucket.org/ccmoura/bicofacil-api/commit/e6f770bb39ba8d5925f7cf3d64760862d573012e))
* **update_client:** implementing rout put ([d321d8b](https://bitbucket.org/ccmoura/bicofacil-api/commit/d321d8b141dfe7ad62a849f9b813037863256d1c))

## [1.13.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.12.0...v1.13.0) (2020-04-09)


### Features

* **provider-store:** i created the function that stores a provider ([e6f770b](https://bitbucket.org/ccmoura/bicofacil-api/commit/e6f770bb39ba8d5925f7cf3d64760862d573012e))

## [1.12.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.11.0...v1.12.0) (2020-04-09)


### Features

* **auth.js:** auth middleware for user authorization ([0461bd1](https://bitbucket.org/ccmoura/bicofacil-api/commit/0461bd1eb3614d0542638e44f89ad5eab1feddb7))

## [1.11.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.10.0...v1.11.0) (2020-04-09)


### Features

* **merge:** merge development - ([aa45d20](https://bitbucket.org/ccmoura/bicofacil-api/commit/aa45d200189cc00fbd127cbe1aff723d638bf3a1))

## [1.10.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.9.0...v1.10.0) (2020-04-09)


### Features

* **client-address:** new columns for cliente address and cep validation ([6ef7797](https://bitbucket.org/ccmoura/bicofacil-api/commit/6ef77977011259fe50c2d702154aeb733b8d39d8))

## [1.9.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.7.0...v1.9.0) (2020-04-07)


### Features

* **file-url:** get file url ([71f715b](https://bitbucket.org/ccmoura/bicofacil-api/commit/71f715b407eff83e6205e7aaf093d48fb72eb97f))
* **get-client:** implementing rout get ([9624e6e](https://bitbucket.org/ccmoura/bicofacil-api/commit/9624e6e578dcbc710f06101c87a72578e90b13b4))

## [1.8.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.2.0...v1.8.0) (2020-04-06)


### Features

* **client.store:** i created the client.store function ([6adbef2](https://bitbucket.org/ccmoura/bicofacil-api/commit/6adbef225102009e899829de5212f1753fa0167e))
* **clientcontroller.js:** functions in the client's controller ([32cb67d](https://bitbucket.org/ccmoura/bicofacil-api/commit/32cb67dddc797913097027c778593d58db26ed18))
* **config/registration.js:** registration configuration file ([2ea2b7f](https://bitbucket.org/ccmoura/bicofacil-api/commit/2ea2b7fe520a34cf74b2c8282e3f38784f1ee7bd))
* **controllers:** creating Controllers ([28a26b2](https://bitbucket.org/ccmoura/bicofacil-api/commit/28a26b26089a0770a61f70d5cf994bdd506d4674))
* **get-client:** implementing rout get ([9624e6e](https://bitbucket.org/ccmoura/bicofacil-api/commit/9624e6e578dcbc710f06101c87a72578e90b13b4))
* **jwt-login:** implementing jsonwebtoken for client login ([46493fe](https://bitbucket.org/ccmoura/bicofacil-api/commit/46493feca1152e123897c692f4542f52ddd1429d))
* **migrations:** creating migrations and uploading images ([6710fce](https://bitbucket.org/ccmoura/bicofacil-api/commit/6710fce69a551bf289c7f4ca5c5fe0417f47321c))


### Bug Fixes

* **file controller import:** removing import for File in controllers ([588a4c0](https://bitbucket.org/ccmoura/bicofacil-api/commit/588a4c09c2ef666d8032822c72a8f906c445ab9a))
* **fixes - from var to const.:** fixes ([af6a48c](https://bitbucket.org/ccmoura/bicofacil-api/commit/af6a48c3d8084d5148238f56065cf97c8cf3b4cd))
* **mudança no app.use(routes);:** mudança no app.use(routes); ([aed8032](https://bitbucket.org/ccmoura/bicofacil-api/commit/aed8032ecc8f4f6f4136db91c5b55908e466a6af))

## [1.7.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.6.0...v1.7.0) (2020-04-06)


### Features

* **config/registration.js:** registration configuration file ([2ea2b7f](https://bitbucket.org/ccmoura/bicofacil-api/commit/2ea2b7fe520a34cf74b2c8282e3f38784f1ee7bd))

## [1.6.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.5.0...v1.6.0) (2020-04-05)


### Features

* **client.store:** i created the client.store function ([6adbef2](https://bitbucket.org/ccmoura/bicofacil-api/commit/6adbef225102009e899829de5212f1753fa0167e))

## [1.5.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.4.0...v1.5.0) (2020-04-05)


### Features

* **clientcontroller.js:** functions in the client's controller ([32cb67d](https://bitbucket.org/ccmoura/bicofacil-api/commit/32cb67dddc797913097027c778593d58db26ed18))

## [1.4.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.3.1...v1.4.0) (2020-04-05)


### Features

* **jwt-login:** implementing jsonwebtoken for client login ([46493fe](https://bitbucket.org/ccmoura/bicofacil-api/commit/46493feca1152e123897c692f4542f52ddd1429d))

### [1.3.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.3.0...v1.3.1) (2020-04-01)


### Bug Fixes

* **file controller import:** removing import for File in controllers ([588a4c0](https://bitbucket.org/ccmoura/bicofacil-api/commit/588a4c09c2ef666d8032822c72a8f906c445ab9a))

## [1.3.0](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.1.2...v1.3.0) (2020-04-01)


### Features

* **controllers:** creating Controllers ([28a26b2](https://bitbucket.org/ccmoura/bicofacil-api/commit/28a26b26089a0770a61f70d5cf994bdd506d4674))
* **migrations:** creating migrations and uploading images ([6710fce](https://bitbucket.org/ccmoura/bicofacil-api/commit/6710fce69a551bf289c7f4ca5c5fe0417f47321c))
* **test route:** a test route has been created ([ed40aa5](https://bitbucket.org/ccmoura/bicofacil-api/commit/ed40aa5c1e0bf88fb9254e80224cfc62cca64abc))


### Bug Fixes

* **fixes - from var to const.:** fixes ([af6a48c](https://bitbucket.org/ccmoura/bicofacil-api/commit/af6a48c3d8084d5148238f56065cf97c8cf3b4cd))
* **mudança no app.use(routes);:** mudança no app.use(routes); ([aed8032](https://bitbucket.org/ccmoura/bicofacil-api/commit/aed8032ecc8f4f6f4136db91c5b55908e466a6af))

>>>>>>> 4de643e13b1af4563301ecd1f81dd5c443ce10fc
## 1.2.0 (2020-03-31)


### Features

* **sequelize:** sequelize ORM added to the project / Suggestion table ([a34f67a](https://bitbucket.org/ccmoura/bicofacil-api/commit/a34f67a94bd4386d3026a9c3c11a17992433df7b))
* **test route:** a test route has been created ([ed40aa5](https://bitbucket.org/ccmoura/bicofacil-api/commit/ed40aa5c1e0bf88fb9254e80224cfc62cca64abc))

>>>>>>> development
### [1.1.3](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.1.2...v1.1.3) (2020-03-30)

### [1.1.2](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.1.1...v1.1.2) (2020-03-30)

### [1.1.1](https://bitbucket.org/ccmoura/bicofacil-api/compare/v1.1.0...v1.1.1) (2020-03-29)

## 1.1.0 (2020-03-29)


### Features

* **sequelize:** sequelize ORM added to the project / Suggestion table ([a34f67a](https://bitbucket.org/ccmoura/bicofacil-api/commit/a34f67a94bd4386d3026a9c3c11a17992433df7b))
