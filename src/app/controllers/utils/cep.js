import cep from 'awesome-cep';
import registration from '../../../config/registration';
export default {
  async checkCEP(zipcode) {
    const spot = await cep.findCEP(zipcode);
    return registration['cities'].find(city => spot.city === city) != undefined
      ? true
      : false;
  },
  async getAddress(zipcode) {
    const {
      city,
      state,
      district,
      address_type,
      address_name,
    } = await cep.findCEP(zipcode);
    return {
      city,
      state,
      district,
      address_type,
      address_name,
    };
  },
};
