import models from '../models';

const Service = models.Service;

class ServiceController {
  async store(request, response) {
    return response.status(200).json({ ok: true });
  },
  async index(request, response) {
    return response.status(200).json({ ok: true });
  },
  async show(request, response) {
    return response.status(200).json({ ok: true });
  },
  async update(request, response) {
    return response.status(200).json({ ok: true });
  },
  async delete(request, response) {
    return response.status(200).json({ ok: true });
  },
  async search(request, response) {
    return response.status(200).json({ ok: true });
  },
}
export default new ServiceController();
