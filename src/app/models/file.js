'use strict';
module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define(
    'File',
    {
      name: DataTypes.STRING,
      path: DataTypes.STRING,
      url: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${process.env.APP_URL}/files/${this.path}`;
        },
      },
    },
    {}
  );
  File.associate = function (models) {
    // associations can be defined here
  };
  return File;
};
