import Joi from '@hapi/joi';
import Moment from 'moment';
import bcrypt from 'bcrypt';

import models from '../models';

import registration from '../../config/registration'
import cpfUtil from './utils/cpf';
import cepUtil from './utils/cep';

const Client = models.Client;
const Provider = models.Provider;

class ClientController {
  async store(request, response) {
    const currentYear = parseInt(Moment().format('YYYY'));
    const schema = Joi.object({
      name: Joi.string().alphanum().min(registration['name'].min).max(registration['name'].max).required(),
      surname: Joi.string().min(registration['surname'].min).max(registration['surname'].max).required(),
      email: Joi.string().email().min(registration['email'].min).max(registration['email'].max).required(),
      password: Joi.string().min(registration['password'].min).max(registration['password'].max).required(),
      repeat_password: Joi.ref('password'),
      cpf: Joi.string().min(11).max(11).required(),
      cep: Joi.string().required(),
      house_number: Joi.number().integer().required(),
      complement: Joi.string().max(40),
      birth_date: Joi.string().regex(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([1][26]|[2468][048]|[3579][26])00))))$/).required(),
      avatar: Joi.string()
    });
    
    const { error } = await schema.validate(request.body);
    const emailExists = await Client.findOne({ where: { email: request.body.email } });

    if(!emailExists){
      emailExists = await Provider.findOne({ where: { email: request.body.email } });
    }

    const age = parseInt(Moment().format('YYYY')) - parseInt(request.body.birth_date.split('/')[2]);
    const cpfExists = await Client.findOne({ where: { cpf: request.body.cpf } });

    if(!(await cpfUtil.checkCPF(request.body.cpf))){
      return response.status(400).json({ error: 'Invalid CPF' })
    } else if(age < 18){
      return response.status(400).json({ error: 'Age must be greater than or equal to 18 years' });
    } else if(error){
      return response.status(400).json({ error });
    } else if(emailExists){
      return response.status(400).json({ error: 'Email already exists' });
    } else if(!(await cepUtil.checkCEP(request.body.cep))){
      return response.status(400).json({ error: 'City not accepted' });
    } else if(cpfExists){
      return response.status(400).json({ error: 'CPF already exists' });
    } else {
      request.body.premium = false;

      const spot = await cepUtil.getAddress(request.body.cep);
      
      request.body.city = spot.city;
      request.body.state = spot.state;
      request.body.district = spot.district;
      request.body.address = `${spot.address_type} ${spot.address_name}`;

      const {
        id, name, surname, email, cep
      } = await Client.create(request.body);
      return response.status(201).json({ id, name, surname, email, cep });
    }
  },

  async index(request, response){
    const users = await Client.findAll();
    return response.status(200).json(users);
  },

  async show(request, response) {
    const user = await Client.findByPk(request.userId || request.params.id, 
      { 
        attributes: [
          'name', 
          'surname',
          'cep',
          'district',
          'state',
          'house_number',
          'complement',
          'email', 
          'address', 
          'premium', 
          'avatar', 
          'createdAt'
        ] 
      });
    
    if (!user) {
      return response.status(404).json({error:'Client not found'});
    } else {
      return response.status(200).json(user);
    }
  },

  async update(request, response) {
    const schema = Joi.object({
      name: Joi.string().alphanum().min(registration['name'].min).max(registration['name'].max).required(),
      surname: Joi.string().min(registration['surname'].min).max(registration['surname'].max).required(),
      cep: Joi.string().required(),
      house_number: Joi.number().integer().required(),
      complement: Joi.string().max(40),
      old_password: Joi.string().min(registration['password'].min).max(registration['password'].max),
      new_password: Joi.string().min(registration['password'].min).max(registration['password'].max),
      repeat_new_password: Joi.ref('new_password'),
      avatar: Joi.string()
    }).with('new_password', 'old_password')
    .with('new_password', 'repeat_new_password');

    const { error } = await schema.validate(request.body);
    const user = await Client.findByPk(request.userId || request.params.id);
    
    if(error){
      return response.status(400).json({ error });
    } else if(request.body.new_password && !(await bcrypt.compare(request.body.old_password, user.password))){
      return response.status(401).json({ error: 'Old password does not match' });
    } else if(!(await cepUtil.checkCEP(request.body.cep))){
      return response.status(400).json({ error: 'City not accepted' });
    } else if(!request.isClient){
      return response.status(401).json({ error: 'Token belongs to a provider' })
    } else{
      const spot = await cepUtil.getAddress(request.body.cep);

      request.body.state = spot.state;
      request.body.district = spot.district;
      request.body.address = `${spot.address_type} ${spot.address_name}`;
      if(request.body.new_password){
        request.body.password = request.body.new_password;
      } 
      const { name, surname, cep, house_number, complement } = await user.update(request.body);

      return response.status(200).json({
        name, surname, cep, house_number, complement 
      });
    }
  },

  async delete(request, response){
    const user = await Client.findByPk(request.params.id);
    const result = await user.destroy();
    return response.status(200).json({ result });
  },
}
export default new ClientController();