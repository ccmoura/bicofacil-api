'use strict';
module.exports = (sequelize, DataTypes) => {
  const Service = sequelize.define(
    'Service',
    {
      title: DataTypes.STRING,
      description: DataTypes.STRING,
      address: DataTypes.STRING,
      photo_1: DataTypes.INTEGER,
      photo_2: DataTypes.INTEGER,
      photo_3: DataTypes.INTEGER,
      provider: DataTypes.INTEGER,
      status: DataTypes.STRING,
    },
    {}
  );
  Service.associate = function (models) {
    Service.hasMany(models.Candidacies, {
      as: 'candidacies',
    });
  };
  return Service;
};
