import Joi from '@hapi/joi';
import Moment from 'moment';
import bcrypt from 'bcrypt';

import models from '../models';

import registration from '../../config/registration';
import cpfUtil from './utils/cpf';
import cepUtil from './utils/cep';
import rolesUtil from './utils/roles';

const Provider = models.Provider;
const Client = models.Client;

class ProviderController {
  async store(request, response) {
    const schema = Joi.object({
      name: Joi.string()
        .alphanum()
        .min(registration['name'].min)
        .max(registration['name'].max)
        .required(),
      surname: Joi.string()
        .min(registration['surname'].min)
        .max(registration['surname'].max)
        .required(),
      email: Joi.string()
        .email()
        .min(registration['email'].min)
        .max(registration['email'].max)
        .required(),
      password: Joi.string()
        .min(registration['password'].min)
        .max(registration['password'].max)
        .required(),
      repeat_password: Joi.ref('password'),
      cpf: Joi.string().min(11).max(11).required(),
      cep: Joi.string().required(),
      house_number: Joi.number().integer().required(),
      complement: Joi.string().max(40),
      birth_date: Joi.string().regex(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([1][26]|[2468][048]|[3579][26])00))))$/).required(),
      experience: Joi.number().integer().min(0).required(),
      roles: Joi.string().required(),
      per_hour: Joi.number().required(),
      phone_number: Joi.string(),
      avatar: Joi.string(),
    });
    const { error } = await schema.validate(request.body);
    const emailExists = await Provider.findOne({ where: { email: request.body.email }});

    if(!emailExists){
      emailExists = await Client.findOne({ where: { email: request.body.email } });
    }

    const age = parseInt(Moment().format('YYYY')) - parseInt(request.body.birth_date.split('/')[2]);
    const cpfExists = await Provider.findOne({ where: { cpf: request.body.cpf } });

    if (error) {
      return response.status(400).json({ error });
    } else if (!(await cpfUtil.checkCPF(request.body.cpf))) {
      return response.status(400).json({ error: 'Invalid CPF' });
    } else if(age < 18){
      return response.status(400).json({ error: 'Age must be greater than or equal to 18 years' });
    } else if (emailExists) {
      return response.status(400).json({ error: 'Email already exists' });
    } else if (!(await rolesUtil.checkRoles(request.body.roles))) {
      return response.status(400).json({ error: 'Invalid role' });
    } else if (!(await cepUtil.checkCEP(request.body.cep))) {
      return response.status(400).json({ error: 'City not accepted' });
    } else if (cpfExists) {
      return response.status(400).json({ error: 'CPF already exists' });
    } else {
      request.body.premium = false;
      request.body.available = true;
      request.body.total_stars = 0;
      request.body.total_services = 0;

      const spot = await cepUtil.getAddress(request.body.cep);
      request.body.state = spot.state;
      request.body.district = spot.district;
      request.body.address = `${spot.address_type} ${spot.address_name}`;

      const { id, name, surname, email, cep, roles } = await Provider.create(
        request.body
      );
      return response.status(201).json({ id, name, surname, email, cep, roles });
    }
  },

  async index (request, response) {
    const users = await Provider.findAll();
    return response.status(200).json(users);
  },

  async delete(request, response) {
    const user = await Provider.findByPk(request.params.id);
    const result = await user.destroy();
    return response.status(200).json({ result });
  },

  async show(request, response) {
    const user = await Provider.findByPk(request.params.id,
      {
        attributes: [
          'name',
          'surname',
          'cep',
          'district',
          'state',
          'house_number',
          'complement',
          'email',
          'address',
          'premium',
          'per_hour',
          'avatar',
          'createdAt',
          'roles',
          'experience',
          'total_services',
          'total_stars',
          'total_ratings'
        ]
      });

      if(!user) {
        return response.status(404).json({error: 'Provider not found'});
      } else {
        user.rating = user.total_ratings == 0 ? 0 : Math.floor(user.total_stars / user.total_ratings);
        return response.status(200).json({user, rating:user.rating});
      }
  },

  async update(request, response) {
    const schema = Joi.object({
      name: Joi.string()
        .alphanum()
        .min(registration['name'].min)
        .max(registration['name'].max)
        .required(),
      surname: Joi.string()
        .min(registration['surname'].min)
        .max(registration['surname'].max)
        .required(),
      cep: Joi.string()
        .required(),
      house_number: Joi.number()
        .integer()
        .required(),
      complement: Joi.string()
        .max(40),
      experience: Joi.number()
        .integer()
        .min(0)
        .required(),
      roles: Joi.string()
        .required(),
      per_hour: Joi.number().required(),
      avatar: Joi.string(),
      available: Joi.boolean()
        .required(),
      old_password: Joi.string()
        .min(registration['password'].min)
        .max(registration['password'].max),
      new_password: Joi.string()
        .min(registration['password'].min)
        .max(registration['password'].max),
      repeat_new_password: Joi.ref('new_password'),
    }).with('new_password', 'old_password')
    .with('new_password', 'repeat_new_password');

    const { error } = await schema.validate(request.body);
    const user = await Provider.findByPk(request.userId);


    if (error) {
      return response.status(400).json({ error });
    } else if (!(await rolesUtil.checkRoles(request.body.roles))) {
      return response.status(400).json({ error: 'Invalid role' });
    } else if (!(await cepUtil.checkCEP(request.body.cep))) {
      return response.status(400).json({ error: 'City not accepted' });
    } else if(request.body.new_password && !(await bcrypt.compare(request.body.old_password, user.password))){
      return response.status(401).json({ error: 'Old password does not match' });
    } else if(request.isClient){
      return response.status(401).json({ error: 'Token belongs to a client' })
    } else {
      const spot = await cepUtil.getAddress(request.body.cep);

      request.body.city = spot.city;
      request.body.state = spot.state;
      request.body.district = spot.district;
      request.body.address = `${spot.address_type} ${spot.address_name}`;
      if(request.body.new_password){
        request.body.password = request.body.new_password;
      } 
      const { name, surname, cep, house_number, complement, experience, roles, available } = await user.update(request.body);

      return response.status(200).json({
        name, surname, cep, house_number, complement, experience, roles, available
      });
    }
  },

  async search(request, response){
    const filter = request.query.filter.toLowerCase();
    const roles = registration['roles'];
    const providers = await Provider.findAll({
      attributes: [
        'name',
        'surname',
        'premium',
        'avatar',
        'roles',
        'experience',
        'total_services',
        'total_stars',
        'total_ratings'
      ],
      raw: true,
    });

    const isRole = roles.filter(role => role.includes(filter));

    if(isRole.length > 0){
      const found = providers.filter(provider => 
        provider.roles.includes(filter) || 
        provider.name.toLowerCase().includes(filter) || 
        provider.surname.toLowerCase().includes(filter) || 
        `${provider.name.toLowerCase()} ${provider.surname.toLowerCase()}`.includes(filter));
      
      found.forEach(provider => {
        provider.rating = provider.total_ratings === 0 ? 0 : provider.total_stars / provider.total_ratings;
        delete provider.total_stars;
        delete provider.total_ratings;
        delete provider.total_services;
      })

      if(found.length > 0){
        response.status(200).json(found);
      } else{
        response.status(404).json(`No provider found with \'${filter}\'`);
      }
    } else {
      const found = providers.filter(provider => 
        provider.name.toLowerCase().includes(filter) || 
        provider.surname.toLowerCase().includes(filter) || 
        `${provider.name.toLowerCase()} ${provider.surname.toLowerCase()}`.includes(filter));
      
      found.forEach(provider => {
        provider.rating = provider.total_ratings === 0 ? 0 : provider.total_stars / provider.total_ratings;
        delete provider.total_stars;
        delete provider.total_ratings;
        delete provider.total_services;
      })
      if(found.length > 0){
        response.status(200).json(found);
      } else{
        response.status(404).json(`No provider found with \'${filter}\'`);
      }
     
    }   
  }
}
export default new ProviderController();
