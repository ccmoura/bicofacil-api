import request from 'supertest';
import app from '../../src/app';
var token;
describe('client routes', () => {
  it('should be able to create the client', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlossva@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '02350502023',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
  });

  it('should be able to update password', async () => {
    const response = await request(app).put('/bicofacil/clients/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      old_password: 'senhadocarlota',
      new_password: 'senhadocarlota2021',
      repeat_new_password: 'senhadocarlota2021',
    });
    expect(response.status).toBe(200);
  });

  it('should not be able to create the client by age', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Josias',
      surname: 'Rosa',
      email: 'emailde_teste@gmail.com',
      password: 'senhadocarinha',
      repeat_password: 'senhadocarinha',
      cpf: '90057899053',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/2005',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe(
      'Age must be greater than or equal to 18 years'
    );
  });

  it('should be able to update cep', async () => {
    const response = await request(app).put('/bicofacil/clients/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
    });
    expect(response.status).toBe(200);
    expect(response.body.cep).toBe('96010750');
  });

  it('should not be able to update password by size', async () => {
    const response = await request(app).put('/bicofacil/clients/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      old_password: 'senhadocarlota',
      new_password: '1234',
      repeat_new_password: '1234',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should not be able to update by city', async () => {
    const response = await request(app).put('/bicofacil/clients/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96225000',
      complement: 'Casa 4w',
      house_number: 150,
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should not be able to update password by old password', async () => {
    const response = await request(app).put('/bicofacil/clients/1').send({
      name: 'Carlos',
      surname: 'Silva',
      cep: '96010750',
      complement: 'Casa 4w',
      house_number: 150,
      old_password: 'senhadocalota',
      new_password: '125792NN',
      repeat_new_password: '125792NN',
    });
    expect(response.status).toBe(401);
    expect(response.body.error).toBe('Old password does not match');
  });

  it('should show info about client', async () => {
    const response = await request(app).get('/bicofacil/clients/1');
    expect(response.status).toBe(200);
  });

  it('should not show info about client', async () => {
    const response = await request(app).get('/bicofacil/clients/12');
    expect(response.status).toBe(404);
  });

  it('should present email exists error', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlossva@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '02350502023',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should present cpf exists error', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '02350502023',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should present city not accepted error', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlosas23va@hotmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '15421387011',
      cep: '20011901',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
  });

  it('should list all registered clients', async () => {
    const response = await request(app).get('/bicofacil/clients');

    expect(response.status).toBe(200);
  });

  it('should present invalid cpf error', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carlossva@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'senhadocarlota',
      cpf: '02000500020',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty('error');
    expect(response.body.error).toBe('Invalid CPF');
  });

  it('should delete the first client', async () => {
    const response = await request(app).delete('/bicofacil/clients/1');

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty('result');
  });

  it('should present schema error on password', async () => {
    const response = await request(app).post('/bicofacil/clients').send({
      name: 'Carlos',
      surname: 'Silva',
      email: 'carloss1341va@gmail.com',
      password: 'senhadocarlota',
      repeat_password: 'snhadocarlota',
      cpf: '68077268056',
      cep: '96020360',
      complement: 'Casa 4w',
      house_number: 150,
      birth_date: '19/10/1991',
    });
    expect(response.status).toBe(400);
  });
});
