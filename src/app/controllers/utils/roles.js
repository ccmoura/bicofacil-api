import registration from '../../../config/registration';
export default {
  async checkRoles(roles) {
    const array = roles.split(';');
    roles.split(';').forEach(role => {
      if (registration['roles'].find(availableRole => role === availableRole)) {
        array.shift();
      }
    });
    return array.length == 0;
  },
};
