'use strict';
import bcrypt from 'bcryptjs';
module.exports = (sequelize, DataTypes) => {
  const Provider = sequelize.define(
    'Provider',
    {
      name: DataTypes.STRING,
      surname: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      cpf: DataTypes.STRING,
      address: DataTypes.STRING,
      cep: DataTypes.STRING,
      city: DataTypes.STRING,
      state: DataTypes.STRING,
      district: DataTypes.STRING,
      house_number: DataTypes.INTEGER,
      complement: DataTypes.STRING,
      premium: DataTypes.BOOLEAN,
      avatar: DataTypes.INTEGER,
      birth_date: DataTypes.STRING,
      total_services: DataTypes.INTEGER,
      experience: DataTypes.INTEGER,
      roles: DataTypes.STRING,
      description: DataTypes.STRING,
      photo_1: DataTypes.INTEGER,
      photo_2: DataTypes.INTEGER,
      photo_3: DataTypes.INTEGER,
      photo_4: DataTypes.INTEGER,
      photo_5: DataTypes.INTEGER,
      total_stars: DataTypes.INTEGER,
      available: DataTypes.BOOLEAN,
      total_ratings: DataTypes.INTEGER,
      per_hour: DataTypes.FLOAT,
      phone_number: DataTypes.STRING,
    },
    {}
  );
  Provider.associate = function (models) {
    Provider.hasMany(models.Service, {
      as: 'services',
    });
    Provider.hasMany(models.Rating, {
      as: 'ratings',
    });
    Provider.hasMany(models.Candidacies, {
      as: 'candidacies',
    });
    this.addHook('beforeSave', async provider => {
      if (provider.password) {
        provider.password = await bcrypt.hash(provider.password, 8);
      }
    });
  };
  return Provider;
};
