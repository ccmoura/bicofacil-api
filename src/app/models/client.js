'use strict';
import bcrypt from 'bcryptjs';
module.exports = (sequelize, DataTypes) => {
  const Client = sequelize.define(
    'Client',
    {
      name: DataTypes.STRING,
      surname: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      cpf: DataTypes.STRING,
      address: DataTypes.STRING,
      cep: DataTypes.STRING,
      city: DataTypes.STRING,
      state: DataTypes.STRING,
      district: DataTypes.STRING,
      house_number: DataTypes.INTEGER,
      complement: DataTypes.STRING,
      premium: DataTypes.BOOLEAN,
      avatar: DataTypes.INTEGER,
      birth_date: DataTypes.STRING,
    },
    {}
  );
  Client.associate = function (models) {
    Client.hasMany(models.Service, {
      as: 'services',
    });
    Client.hasMany(models.Rating, {
      as: 'ratings',
    });

    this.addHook('beforeSave', async client => {
      if (client.password) {
        client.password = await bcrypt.hash(client.password, 8);
      }
    });
  };
  return Client;
};
